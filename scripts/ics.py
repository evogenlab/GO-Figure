#!/usr/bin/env python3
import sys
import gzip
from collections import defaultdict
from math import log10

# check whether GOA file was provided as compressed file
goa_open = gzip.open if sys.argv[2].endswith('.gz') else open

goChildren = defaultdict(list)
goParents = defaultdict(list)
goCounts = defaultdict(int)
namespaceDict = defaultdict(str)

goSet = set()
for line in open(sys.argv[1]): ## GO parents
	if not line.startswith('#'):
		go,parent = line.strip().split('\t')
		goSet.add(go)
		goSet.add(parent)
		goParents[go].append(parent)

for line in open(sys.argv[1]): ## GO children
	if not line.startswith('#'):
		child,go = line.strip().split('\t')
		goChildren[go].append(child)

for line in open(sys.argv[3]):
	if line.startswith('id: GO:'):
		go = line.strip().split('id: ')[1]
	elif line.startswith('namespace: '):
		namespace = line.strip().split('namespace: ')[1]
		namespaceDict[go] = namespace

totMF = 0
totBP = 0
totCC = 0
for line in goa_open(sys.argv[2], 'rt'):
	if not line.startswith('!'):
		ssline = line.strip().split('\t')
		go = ssline[4]
		goCounts[go] += 1
	else:
		if line.startswith('!Generated: '):
			goa_version = line.strip().split(' ')[1]
			print('# GOA version used: '+goa_version)

for go,count in goCounts.items():
	if namespaceDict[go] == 'biological_process':
		totBP += count
	elif namespaceDict[go] == 'molecular_function':
		totMF += count
	elif namespaceDict[go] == 'cellular_component':
		totCC += count

goCountsDownpropagated = defaultdict(int)
for go in goSet:
	counts = goCounts[go]
	goCountsDownpropagated[go] = counts
	for child in goChildren[go]:
		goCountsDownpropagated[go] += goCounts[child]

print('#GO\tIC\tFrequency')
for go in goCountsDownpropagated:
	if namespaceDict[go] == 'biological_process':
		tot = totBP
	elif namespaceDict[go] == 'molecular_function':
		tot = totMF
	elif namespaceDict[go] == 'cellular_component':
		tot = totCC
	ic = 0
	frequency = 0
	if not goCountsDownpropagated[go] == 0:
		ic = -log10(goCountsDownpropagated[go]/tot)
		frequency = goCountsDownpropagated[go]/tot
	else:
		ic = -log10(1/tot)
		frequency = 1/tot
	print(go+'\t'+str(ic)+'\t'+str(frequency))
